# Instalacja dependencji:
 - kompilator *g++* lub *clang++* z *objcopy*
 - *mformat* i *mcopy* z pakietu *mtools* 
 - *qemu* a dokładniej *qemu-system-x86_64* 
 -  *cmake* do budowania
 - biblioteka [gnu-efi](https://sourceforge.net/projects/gnu-efi/files/latest/download)
## Instalacja na debian / ubuntu i pochodne:
    sudo apt update
    sudo apt install qemu-kvm qemu-system mtools cmake g++ make build-essential
Jeśli chcesz przejść do części wykorzystującej automatyczną kompilacje za pomocą cmake'a i nie chcesz czytać o ręcznej kompilacji kliknij [tutaj](#automatyzacja-procesu)
# Kompilacja potrzebnych bibliotek
## gnu-efi
Po pobraniu z linku i rozpakowaniu wykonujemy `make` i po zakończeniu kopiujemy:
 - Katalog `inc` z plikami nagłówkowymi w repozytorium do `inc/gnu-efi`
 - Pliki `./x86_64/lib/libefi.a` i `./x86_64/gnuefi/libgnuefi.a` zastępują nam standardową bibliotekę języka C
 - Plik `./x86_64/gnuefi/crt0-efi-x86_64.o`  zastapi nam kod który normalnie wykonuje się przed funkcją "main" i inicjalizuje bibliotekę języka C. U nas zainicjalizuję bibliotekę libefi.
 - Plik `./gnuefi/elf_x86_64_efi.lds` jest skryptem linkera zawierającym opis struktury pliku efi ( adresy sekcji, adresy tablicy z symbolami itd)
## [TianoCore](https://www.tianocore.org/)
Do uruchomiania aplikacji w emulatorze bedziemy potrzebować biosu ( UEFI ) do qemu. W tym celu możemy wykorzystać TianoCore z projektu [edk2](https://github.com/tianocore/edk2). W repozytorium znajduje się już skompilowane TianoCore jako `bios/OVMF.fd`. Intrukcje do budowania edk2 można znaleźć na [https://github.com/tianocore/tianocore.github.io/wiki/Common-instructions](https://github.com/tianocore/tianocore.github.io/wiki/Common-instructions). A po kompilacji skpiować plik OVMF.fd.
# Struktura katalogowa projektu
 - `bios/OVMF.fd`   skompilowane TianoCore
 - `inc`  katalog z plikami nagłówkowymi
 - `inc/gnu-efi` zawartość katalogu `inc` z gnu-efi
 - `lib` skompilowana biblioteka gnu-efi: `libefi.a` `libgnuefi.a` `crt0-efi-x86_64.o`
 - `scripts` skrypty do linkera miedzy innymi `elf_x86_64_efi.lds`
- `src` do plików źródłowych
# Plik `main.cc`
Biblioteka gnu-efi jest przystosowana domyślnie do języka C i nie obsługuje oficjalnie języka C++ z tego względu oficjalny "Hello World" wygląda tak:
```c
#include <efi.h>
#include <efilib.h>

EFI_STATUS
EFIAPI
efi_main (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable) {
   InitializeLib(ImageHandle, SystemTable);
   Print(L"Hello, world!\n");

   return EFI_SUCCESS;
}
```
Aby pisać w C++ musimy upewnić się że podczas kompilacji mangler nie pozamienia nazw funckji `efi_main`, `InitializeLib` i `Print` na ich C++'sowe wersje, gdzyż wtedy nie zostaną połączone na etapie konsolidacji z statyczną biblioteką `libefi.a` i `libgnuefi.a `, która zawiera nazwy funkcji z C nie poddane manglingu z C++. Aby temu zapobiec wystarczy oznaczyć kompilatorowi C++ wszystko co ma traktować jako kod C w 
```c++
extern "C" {
	// kod z C
}
```
Po modyfikacjach:
```c++
extern "C" {

	#include <efi.h>
	#include <efilib.h>

}

EFI_STATUS cxx_main();

extern "C" {

	EFI_STATUS
	EFIAPI
	efi_main (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable) {
	   InitializeLib(ImageHandle, SystemTable);
	   SystemTable->BootServices->SetWatchdogTimer(0, 0, 0, nullptr); // Disable UEFI's timer which after ~5min would reset the computer.
	   return cxx_main();
	}

}

EFI_STATUS cxx_main() {
    // Insert your C++ code here
    Print((CHAR16*)L"Hello World.\n");
    // ------------------------
    return EFI_SUCCESS;
}
```
W funckji `cxx_main` możemy już używać języka C++.
# Kompilacja aplikacji
## 1. Kompilacja do plików obiektowych:
Każdy plik źródłowy kompilujemy następującym poleceniem:
```
g++ main.cc                              \
      -c                                 \
      -fno-stack-protector               \
      -fpic                              \
      -fshort-wchar                      \
      -mno-red-zone                      \
      -I inc                             \
      -I inc/gnu-efi                     \
      -I inc/gnu-efi/x86_64              \
      -DEFI_FUNCTION_WRAPPER             \
      -o main.o
```
## 2.Konsolidacja
Uzyskane w ten sposób pliki obiektowe linkujemy w dynamiczną bibliotekę następującym poleceniem:
```
ld main.o                         \
     lib/crt0-efi-x86_64.o          \
     -nostdlib                      \
     -znocombreloc                  \
     -T scripts/elf_x86_64_efi.lds  \
     -shared                        \
     -Bsymbolic                     \
     -L lib                         \
     -l:libgnuefi.a                 \
     -l:libefi.a                    \
     -o main.so
```
Oczywiście oprócz pliku `main.o` podajemy wszystkie pliki obiektowe uzyskane w 1 etapie kompilacji
## 3. Konwersja pliku .so na .efi
Uzyskany w 2 etapie plik `.so` bedący dynamiczną biblioteką systemu linux musimy jeszcze przerobić na plik `.efi` aby mógł być uruchomiony pod UEFI:
```
objcopy -j .text                \
          -j .sdata               \
          -j .data                \
          -j .dynamic             \
          -j .dynsym              \
          -j .rel                 \
          -j .rela                \
          -j .reloc               \
          --target=efi-app-x86_64 \
          main.so                 \
          main.efi
```
Uzyskany w ten sposób plik `main.efi` jest już gotową aplikacją, którą można uruchomić pod UEFI.
# Uruchomienie
## 1. Na "Bare-metal" czyli przez zabootowanie komputera z pendriva zawierającego naszą aplikacje
Do uruchomienia wystarczy skopiować uzyskany wczesniej plik `main.efi` na pendrive'a sformatowanego jako `FAT32` do katalogu `EFI/BOOT` jako `BOOTX64.efi`
```
cp main.efi <ścieżka do pendrive'a>/EFI/BOOT/BOOTX64.efi
```
Następnie uruchamiamy komputer z tego pendriva. Podczas próby można natrafić na problemy związane z opcją [`Secure Boot`](https://wiki.osdev.org/UEFI#Secure_Boot), która przed uruchomieniem aplikacji efi sprawdza czy została ona podpisana cyfrowo. Oczywiście nasza aplikacja nie jest podpisana, dlatego gdy pojawią się problemy z bootowaniem należy upewnić się, czy ta opcja została wyłączona w biosie.
## 2. Pod QEMU
### 1. Tworzenie obrazu partycji EFI
Aby uruchomić aplikację pod qemu musimy stworzyć obraz dysku zawierający partycje EFI (partycje z systemem plików `FAT32`) z naszą aplikacją.

Tworzenie obrazu:
```
dd if=/dev/zero of=uefi.img bs=512 count=93750
sudo parted uefi.img -s -a minimal mklabel gpt
sudo parted uefi.img -s -a minimal mkpart EFI FAT16 2048s 93716s
sudo parted uefi.img -s -a minimal toggle 1 boot
dd if=/dev/zero of=/tmp/part.img bs=512 count=91669
mformat -i /tmp/part.img -h 32 -t 32 -n 64 -c 1
mcopy -i /tmp/part.img main.efi ::
```
Do obrazu można też dodać plik `startup.nsh` aby TianoCore od razu uruchomiło nasz aplikacje po starcie. Jest to odpowiednik "autoexec.bat" pod DOS'em.
```
echo main.efi > startup.nsh
mcopy -i /tmp/part.img startup.nsh ::
```
I na koniec przepisać partycje EFI na dysk z odpowiednim offsetem:
```
dd if=/tmp/part.img of=uefi.img bs=512 count=91669 seek=2048 conv=notrunc
```
Uzyskujemy w ten sposób gotowy obraz dysku do QEMU
### 2. Uruchomienie 
Po przygotowaniu obrazu wystarczy wywołać polecenie
```
qemu-system-x86_64  -bios bios/OVMF.fd -drive file=uefi.img
```
Jeżeli do obrazu dodano `startup.nsh` wystarczy poczekać aż TianoCore uruchomi skrypt. W przeciwnym przypadku należy przejść do `fs0` i uruchomić naszą aplikacje:
```
fs0:
main.efi
```
Lub krócej:
```
fs0:main.efi
```
# Automatyzacja procesu
W repozytorium znajduje się plik `CMakeLists.txt`, który dzieki narzędziu `cmake` automatyzuje kompilacje, konsolidacje, tworzenie obrazu i uruchamianie qemu.
```
git clone https://gitlab.com/L0czek/efi-template.git
cd efi-template
mkdir build
cd build
cmake ..
```
Po wywołaniu :
```
make
```
w głównym katalogu repozytorium utworzony zostanie plik `BOOTX64.efi`, który możemy skopiowac na pendrive'a. W celu utworzenia obrazu do QEMU w katalogu `build` wykonujemy:
```
make img
```
wtedy w głównym katalogu powstanie obraz dysku o domyślnej nazwie `disk.img`. Nazwy plików wynikowych można zmienić w pliku `CMakeLists.txt` modyfikujac nazwy odpowiednich zmiennych.

QEMU uruchamiamy poprzez wywołanie w katalogu `build`
```
make run
```
# Z czego (nie-)można korzystać
Podczas pisania kodu należy pamiętać o braku standardowej biblioteki języka C przez co nie możemy korzystać np. z `malloc` i `free`. Skutkuje to niemożnością korzystania w C++ ze standardowych kontenerów takich jak: `std::vector`, `std::map`, czy `std::string`. Każdy z nich korzysta z`std::allocator` do alokacji pamięci, który z kolei wywołuje globalne `new` i `delete` , które ostatecznie korzystają z `malloc` i `free`.  Aby móc z nich skorzystać musimy więc dostraczyć własną implementacje sterty oraz  `malloc`, `free` i własny `std::allocator`. Przykładową implementacje można znaleźć w repozytorium [soi_challenge_1](https://gitlab.com/L0czek/soi_challenge_1) w plikach `sheap.hpp` i `sheap.cc`. 
Możemy za to korzystać ze wszystkiego co zawiera się w bibliiotece standardowej tylko w plikach nagłówkowych i nie odwołuje się do funckji / zmiennych, które są już w plikach źródłowych. Możemy zatem bez obawy korzystać z constexpr'owych templatek. 

Dostępne są też różne funkcje udostępnianie przez UEFI:
1. W grupie [RuntimeServices](https://doxygen.reactos.org/d0/d39/structEFI__RUNTIME__SERVICES.html) poprzez globalny pointer `ST->RuntimeServices`
2. W grupie [BootServices](https://doxygen.reactos.org/d7/d9a/structEFI__BOOT__SERVICES.html) poprzez globalny pointer `BS`
3. Inne funkcje dostarczane przez gnu-efi. Tutaj nie mam już linku do dokumentacji bo takowej nie ma. Zainteresowanym zalecam przejrzeć pliki nagłówkowe.

Globalne pointery `ST` i `BS` są ustawiane przez funkcję `InitializeLib` w `efi_main`.