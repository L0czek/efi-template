extern "C" {

	#include <efi.h>
	#include <efilib.h>

}

EFI_STATUS cxx_main();

extern "C" {

	EFI_STATUS
	EFIAPI
	efi_main (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable) {
	   InitializeLib(ImageHandle, SystemTable);
	   SystemTable->BootServices->SetWatchdogTimer(0, 0, 0, nullptr); // Disable UEFI's timer which after ~5min would reset the computer.
	   return cxx_main();
	}

}

EFI_STATUS cxx_main() {
    // Insert your C++ code here
    Print((CHAR16*)L"Hello World.\n");
    // ------------------------
    return EFI_SUCCESS;
}
